<?php
// floatval() function
$a = "1234.55 Hello";
echo floatval($a);
echo "<br>";
$a = "Hello 1234.55 ";
echo floatval($a);
echo "<br>";

//var_dump() function
$s = "Sahela";
echo var_dump($s);
echo "<br>";
//empty function,return boolean result

$a= "";
echo empty($a);
echo "<br>";
 //is-array function,return boolean result
$a = array('1','2','3');
echo is_array($a);
echo "<br>";
//is_null function,boolean result
$a = NULL;
echo var_dump(is_null($a));
echo "<br>";
//isset funtion,bbolean result
$a=NULL;
$b="hello";
$c='';
echo var_dump(isset($a))."<br>";
echo var_dump(isset($b))."<br>";
echo var_dump(isset($b))."<br>";
//print_r func
$a = array('1','2','3');
print_r($a);
echo "<br>";
//serialize
$a = serialize(array('1','2','3'));
echo $a;
echo "<br>";

//unserialize
$t= unserialize($a);
var_dump($t);
echo "<br>";


//unset function
$a = "Hello";
var_dump($a);
unset($a);
echo "<br>";
var_dump($a);
echo "<br>";
//var_dump function
$a = 1;
var_dump($a);
echo "<br>";
$a = 11.2;
var_dump($a);
echo "<br>";
$a = "Sahela";
var_dump($a);
echo "<br>";
$a = true;
var_dump($a);
echo "<br>";
//gettype
$a = "sahela";
echo gettype($a);
echo "<br>";
//is_bool,boolean result
$a = true;
$b = false;
$c =11;
var_dump(is_bool($a));
echo "<br>";
var_dump(is_bool($c));
echo "<br>";
var_dump(is_bool($b));
echo "<br>";
//boolbal

$a = 45;
$b =0;
var_dump(boolval($a));
echo "<br>";
var_dump(boolval($b));
echo "<br>";

//intval
$a ="12 hello";
$b="12.5 hello";
$c= "Hello 12";
$d="hello 12.56";
echo intval($a);
echo "<br>";
echo intval($b);
echo "<br>";
echo intval($c);
echo "<br>";
echo intval($d);
echo "<br>";
//is_scalar
$a =12.5;
$b=12;
$c= "Hello 12";
$d=false;
$e=array('1','2','3');
var_dump(is_scalar($a)) ;
echo "<br>";
var_dump(is_scalar($b)) ;
echo "<br>";
var_dump(is_scalar($c)) ;
echo "<br>";
var_dump(is_scalar($d)) ;
echo "<br>";
var_dump(is_scalar($e)) ;
echo "<br>";

//var_export
$a = array (1, 2,3,'5');
var_export($a);
echo "<br>";
$b ="Hello";
var_export($b);
echo "<br>";
$b =124646;
var_export($b);
echo "<br>";